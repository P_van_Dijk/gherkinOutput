package model;

import java.util.List;

/**
 * Created by pvandijk on 2/16/17.
 */
public interface DatabaseInterface {

    void createSchema();

    void saveTags(List<FunctionTag> functionandtagslist);

    List<FunctionTag> findTag(String tag);

}
