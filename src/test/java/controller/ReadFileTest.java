package controller;

import model.FileToRead;
import org.junit.Test;

import java.io.File;
import java.net.URL;

import static org.junit.Assert.*;

/**
 * Created by pvandijk on 3/24/17.
 */
public class ReadFileTest {
    @Test
    public void ReadExistingFile() throws Exception {
        //Get path for text file
        URL url = Thread.currentThread().getContextClassLoader().getResource("Readfile.txt");
        File file = new File(url.getPath());

        FileToRead f = new FileToRead();
        f.setFile(file.getAbsolutePath());
        String s = ReadFile.getFileContent(f);

        assertEquals("[File read, succesfull!]", s);
    }

    @Test
    public void ReadNoneExistingFile() throws Exception {

        FileToRead f = new FileToRead();
        f.setFile("NoneExistingFile.txt");
        String s = ReadFile.getFileContent(f);

        assertEquals("[]", s);
        System.out.println(s);
    }



}