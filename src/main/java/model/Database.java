package model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Pedro on 2/2/2017.
 */
//SpringBootApplication

@Repository
public class Database implements DatabaseInterface {
    private JdbcTemplate jdbcTemplate;


    public Database(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @PostConstruct
    @Override
    public void createSchema() {
        jdbcTemplate.execute("DROP TABLE tags IF EXISTS");
        jdbcTemplate.execute("CREATE TABLE tags("
                + "functionName VARCHAR(255), tag VARCHAR(255))");
    }

    @Override
    public void saveTags(List<FunctionTag> functionandtagslist){
        System.out.println("Method saveTags invoked");
        for (FunctionTag functionandtag : functionandtagslist) {
            jdbcTemplate.update("INSERT INTO tags (functionName,tag) VALUES (?,?)",
                    new Object[]{functionandtag.getFunctionName(), functionandtag.getTag()});
        }
    }


    public List<FunctionTag> findTag(String tag){
        System.out.println("Try to find tag in database");
        List<FunctionTag> functionTags = new ArrayList<FunctionTag>();

            jdbcTemplate.query(
                "SELECT functionName, tag FROM tags WHERE tag = ?", new Object[] { tag },
                (rs, rowNum) -> new FunctionTag(rs.getString("functionName"), rs.getString("tag"))
            ).forEach(functionTag -> functionTags.add(functionTag) );

        for (FunctionTag functionTag : functionTags) {
            System.out.println("found " + functionTags);
        }

        return functionTags;
    }


}
