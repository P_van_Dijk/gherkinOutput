package controller;

import model.FileToRead;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadFile {

    public static String getFileContent(FileToRead filepath) {

        System.out.println("getFileContent method invoked");

        List<String> list = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(filepath.getFile()))) {
            list = stream.collect(Collectors.toList());
        } catch (IOException e) { filepath.setFile("Error: File not found - " + filepath.getFile());}

        return list.toString();
    }

}