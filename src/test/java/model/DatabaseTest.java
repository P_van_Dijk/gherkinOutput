package model; /**
 * Created by pvandijk on 3/21/17.
 */
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import controller.RestController;
import model.Database;
import model.DatabaseInterface;
import model.FileToRead;
import model.FunctionTag;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.util.ArrayList;
import java.util.List;

public class DatabaseTest {

    @Test
    public void databaseStoreAndFindFunctionTags() {
        EmbeddedDatabase db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .build();
        JdbcTemplate j = new JdbcTemplate(db);
        Database d = new Database(j);
        d.createSchema();

        List<FunctionTag> functionandtagslist = new ArrayList<FunctionTag>();
        functionandtagslist.add(new FunctionTag("func1", "vasheet"));
        functionandtagslist.add(new FunctionTag("func2", "pva"));
        functionandtagslist.add(new FunctionTag("func31~2`#$)", "booking"));

        d.saveTags(functionandtagslist);

        assertEquals("[FunctionTag[functionName='func1', tag='vasheet']]", d.findTag("vasheet").toString());
        assertEquals("[FunctionTag[functionName='func31~2`#$)', tag='booking']]", d.findTag("booking").toString());
        assertEquals("[FunctionTag[functionName='func2', tag='pva']]", d.findTag("pva").toString());

    }

}
