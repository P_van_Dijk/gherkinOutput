package model;

public class FunctionTag {
    private String functionName, tag;

    public FunctionTag() {

    }

    public FunctionTag(String functionName, String tag) {
        this.functionName = functionName;
        this.tag = tag;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        return String.format(
                "FunctionTag[functionName='%s', tag='%s']",
                functionName, tag);
    }

}
