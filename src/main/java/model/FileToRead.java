package model;

public class FileToRead {

    private String filepath;

    public String getFile() {
        return filepath;
    }

    public void setFile(String filepath) {
        this.filepath = filepath;
    }

}
