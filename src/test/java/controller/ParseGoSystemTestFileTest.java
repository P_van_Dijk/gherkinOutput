package controller; /**
 * Created by pvandijk on 3/21/17.
 */
import junit.framework.Assert;
import model.FunctionTag;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertNull;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class ParseGoSystemTestFileTest {

    @Test
    public void getTestFunctionName() throws Exception {

        String testFunctionName = controller.ParseGoSystemTestFile.getTestFunctionName(")\n" +
                "\n" +
                "//Tags: vasheet, multiple periods, pva\n" +
                "func TestVAExportSimpleScenario(t *testing.T) {\n" +
                "\tCleanTables(t)");
        assertEquals("VAExportSimpleScenario",testFunctionName);
    }

    @Test
    public void getTestFunctionNameEmptyFile() throws Exception {
        //Empty file
        String testFunctionName = controller.ParseGoSystemTestFile.getTestFunctionName("");
        assertEquals("",testFunctionName);
    }

    @Test
    public void getFunctionNameAndTags() throws Exception {
        List<FunctionTag> testFunctionNameAndTags = controller.ParseGoSystemTestFile.getFunctionNameAndTags(")\n" +
                "\n" +
                "//Tags: vasheet ,  multiple periods  , pva \n" +
                "func TestVAExportSimpleScenario(t *testing.T) {\n" +
                "\tCleanTables(t)");
        for (FunctionTag testFunctionNameAndTag : testFunctionNameAndTags) {
            assertEquals("VAExportSimpleScenario", testFunctionNameAndTag.getFunctionName());
        }

        assertEquals(3,testFunctionNameAndTags.size());

        assertEquals("vasheet", testFunctionNameAndTags.get(0).getTag());
        assertEquals("multiple periods", testFunctionNameAndTags.get(1).getTag());
        assertEquals("pva", testFunctionNameAndTags.get(2).getTag());
    }

    @Test
    public void getFunctionNameAndTagsEmptyFile() throws Exception {
        List<FunctionTag> testFunctionNameAndTags = controller.ParseGoSystemTestFile.getFunctionNameAndTags("");
        assertEquals("", testFunctionNameAndTags.get(0).getFunctionName());
        assertEquals("", testFunctionNameAndTags.get(0).getTag());
    }

}
