This program helps to find test about a certain issue. The current version is made to read test tags. 

Above each test you can add tags by adding the following comment:
//Tags:
Followed by tags:
//Tags: vasheet, pva
 
You can start the app by starting the StartApp class.

Thereafter, you need to browse to: http://localhost:8080/readfile
There you give the path to a test file. You can find an example file in the resources named cis_report_test.txt
After submitting the file, you can search for tags with the REST call: http://localhost:8080/findTestFunctions you should give the call a "tag" attribute
When you use the example file, then you could use the following call http://localhost:8080/findTestFunctions?tag=vasheet

This should give you the following result: [{"functionName":"VAExportSimpleScenario","tag":"vasheet"}]

So, it returns you all functions that have the vasheet tag.

At the moment it can not read more than one function. It would be easy to extend this. However, I prefer to first have better testing in place.



 