package start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by pvandijk on 24-1-2017.
 */
@SpringBootApplication
@ComponentScan("model")
@ComponentScan("start")
@ComponentScan("controller")
public class StartApp {

    public static void main(String[] args) {
        SpringApplication.run(StartApp.class, args);
    }



    //Start de database op.
    //Test connectie

    //Website met form om de local url te versturen waar de file staat die uitgelezen moet worden
    //Optie: zip file uploaden

    //Lezen file
    //Misschien moet je niet de classloader gebruiken, maar gewoon een inputstream
    //Wegschrijven naar de database

    //Rest call leest database uit

}
