package controller;

import model.Database;
import model.DatabaseInterface;
import model.FileToRead;
import model.FunctionTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class RestController {

    private Database databaseInterface;

    @Autowired
    public RestController(Database databaseInterface) {
        this.databaseInterface = databaseInterface;
    }

    @GetMapping("/readfile")
    public String readfileForm(Model model) {
        System.out.println("readfileForm Rest call invoked");
        model.addAttribute("readfile", new FileToRead());
        return "readfile";
    }

    @PostMapping("/readfile")
    public String readSubmit(@ModelAttribute FileToRead fileToRead) {
        System.out.println("readSubmit Rest call invoked");
        databaseInterface.saveTags (ParseGoSystemTestFile.getFunctionNameAndTags(ReadFile.getFileContent(fileToRead)));
        return "result";
    }

    @RequestMapping(value="/findTestFunctions", method = RequestMethod.GET)
    @ResponseBody
//    http://localhost:8080/findTestFunctions?tag=vasheet
    public List<String> findTestFunctions(@RequestParam("tag") String tag){
        return databaseInterface.findTag(tag)
                .stream()
                .map(FunctionTag::getFunctionName)
                .collect(Collectors.toList());
    }


}
