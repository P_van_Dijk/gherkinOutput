package controller;

import model.FunctionTag;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pedro on 1/30/2017.
 */
public class ParseGoSystemTestFile {

    public static String getTestFunctionName (String GoTestFile){
        String upper = GoTestFile.toUpperCase();
        String funcName;

        try {
            //Get test function name
            int indexStartFuncName = upper.indexOf("FUNC TEST");
            int indexEndFuncName = upper.indexOf("(",indexStartFuncName);
            //System.out.println("indexStartFunc:" + indexStartFunc);
            //System.out.println("indexEndFunc:" + indexEndFunc);
            funcName = GoTestFile.substring(indexStartFuncName +9,indexEndFuncName);
        } catch (IndexOutOfBoundsException e) {
            funcName = "";
        }

        return funcName;
    }


    public static List<FunctionTag> getFunctionNameAndTags (String GoTestFile){
        String upper = GoTestFile.toUpperCase();
        String tags;
        String funcName = getTestFunctionName(GoTestFile);


        //Get tags
        try {
            int indexStartTags = upper.indexOf("//TAGS:");
            int indexEndTags = upper.indexOf("FUNC", indexStartTags);
            tags = GoTestFile.substring(indexStartTags + 8, indexEndTags - 1);
        } catch (IndexOutOfBoundsException e) {
            tags = "";
        }

        String[] tagslist = tags.split(",");

        List<FunctionTag> functionandtagslist = new ArrayList<FunctionTag>();
        for (String tag : tagslist) {
            functionandtagslist.add(new FunctionTag(funcName, tag.trim().replaceAll(" +", " ")));
        }

        return functionandtagslist;
    }

}
